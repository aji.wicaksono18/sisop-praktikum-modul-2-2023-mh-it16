#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <signal.h>
#include <time.h>
#include <syslog.h>

int tingkat_keamanan = 0;
char **ekstensi_diperbolehkan = NULL;
int num_ekstensi = 0;

void pindahFile(const char* nama_file) {
    char path_asal[256];
    char path_tujuan[256];

    snprintf(path_asal, sizeof(path_asal), "sisop_infected/%s", nama_file);
    snprintf(path_tujuan, sizeof(path_tujuan), "karantina/%s", nama_file);

    if (rename(path_asal, path_tujuan) != 0) {
        perror("Error dalam memindahkan file");
    } else {
        printf("Memindahkan file: %s\n", nama_file);
    }
}

void hapusFile(const char* nama_file) {
    char path_file[256];
    snprintf(path_file, sizeof(path_file), "sisop_infected/%s", nama_file);

    if (remove(path_file) != 0) {
        perror("Error dalam menghapus file");
    } else {
        printf("Menghapus file: %s\n", nama_file);
    }
}

void tambahLog(const char* nama_file, const char* aksi) {
    time_t waktu_sekarang;
    struct tm *info;
    time(&waktu_sekarang);
    info = localtime(&waktu_sekarang);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", info);

    char tingkat_keamanan_str[20];
    if (tingkat_keamanan == 0) {
        strcpy(tingkat_keamanan_str, "rendah");
    } else if (tingkat_keamanan == 1) {
        strcpy(tingkat_keamanan_str, "menengah");
    } else if (tingkat_keamanan == 2) {
        strcpy(tingkat_keamanan_str, "tinggi");
    }

    FILE *logFile = fopen("virus.log", "a");
    if (logFile) {
        fprintf(logFile, "[%d][%s] - %s - %s. (untuk tingkat %s)\n", getpid(), timestamp, nama_file, aksi, tingkat_keamanan_str);
        fclose(logFile);
    } else {
        perror("Error membuka virus.log");
    }
}

void handleSignal(int signum) {
    char tingkat_keamanan_baru[20];
    if (signum == SIGUSR1) {
        tingkat_keamanan = 0;
        strcpy(tingkat_keamanan_baru, "rendah");
    } else if (signum == SIGUSR2) {
        tingkat_keamanan = 1;
        strcpy(tingkat_keamanan_baru, "menengah");
    } else if (signum == SIGRTMIN) {
        tingkat_keamanan = 2;
        strcpy(tingkat_keamanan_baru, "tinggi");
    } else if (signum == SIGTERM) {
        exit(0);
    }

    char pesan_log[100];
    snprintf(pesan_log, sizeof(pesan_log), "Tingkat keamanan diganti menjadi %s", tingkat_keamanan_baru);

    tambahLog("sistem", pesan_log);
}

void bacaFileEkstensi() {
    FILE *file_ekstensi = fopen("ekstensi.csv", "r");
    if (file_ekstensi) {
        char line[256];
        while (fgets(line, sizeof(line), file_ekstensi)) {
            char *ekstensi = strtok(line, ",");
            if (ekstensi) {
                num_ekstensi++;
                char **temp = (char **)realloc(ekstensi_diperbolehkan, num_ekstensi * sizeof(char *));
                if (temp) {
                    ekstensi_diperbolehkan = temp;
                    ekstensi_diperbolehkan[num_ekstensi - 1] = strdup(ekstensi);
                } else {
                    perror("Error dalam alokasi ulang memori");
                    exit(1);
                }
            }
        }
        fclose(file_ekstensi);
    } else {
        perror("Error membuka ekstensi.csv");
    }
}

int isEkstensiDiperbolehkan(const char *ekstensi) {
    for (int i = 0; i < num_ekstensi; i++) {
        if (strcmp(ekstensi, ekstensi_diperbolehkan[i]) == 0) {
            return 1;
        }
    }
    return 0;
}

int main(int argc, char *argv[]) {
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    openlog("antivirus", LOG_PID, LOG_DAEMON);

    
    if (argc != 3) {
        printf("Penggunaan: ./antivirus -p (tingkat_keamanan)\n");
        printf("Tingkat keamanan: 0 (rendah), 1 (menengah), 2 (tinggi)\n");
        return 1;
    }

    tingkat_keamanan = atoi(argv[2]);

    signal(SIGUSR1, handleSignal);
    signal(SIGUSR2, handleSignal);
    signal(SIGRTMIN, handleSignal);
    signal(SIGTERM, handleSignal);

    bacaFileEkstensi();

    while (1) {
        struct dirent *entry;
        DIR *dir = opendir("sisop_infected");

        if (dir == NULL) {
            perror("Error dalam membuka direktori");
            return 1;
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) {
                char *nama_file = entry->d_name;
                char *ekstensi = strrchr(nama_file, '.');

                if (ekstensi != NULL) {
                    ekstensi++;
                }

                if (tingkat_keamanan == 0) {
                    tambahLog(nama_file, "Me-log file.");
                } else if (tingkat_keamanan == 1) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        pindahFile(nama_file);
                        tambahLog(nama_file, "Dipindahkan ke karantina.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                } else if (tingkat_keamanan == 2) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        hapusFile(nama_file);
                        tambahLog(nama_file, "Menghapus file.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                }
            }
        }

        closedir(dir);

        sleep(1);
    }

    for (int i = 0; i < num_ekstensi; i++) {
        free(ekstensi_diperbolehkan[i]);
    }
    free(ekstensi_diperbolehkan);

    closelog();

    return 0;
}
