#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <dirent.h>


void buatFolder() {
    int ret = mkdir("players", 0755);

    if (ret == 0) {
        printf("Folder 'players' telah berhasil dibuat.\n");
    } else {
        perror("Gagal membuat folder 'players'");
        exit(1);
    }
}
 
void downloadFile() {
    pid_t pid_donlod= fork();
    int status;

    if (pid_donlod == 0) { 
        execl("/usr/bin/wget", "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL);

    } else if (pid_donlod > 0) { 
        waitpid(pid_donlod, &status, 0);
    }
}

void unzipFile() {
    pid_t pid_unzip = fork();
    int ext_status;

    if (pid_unzip == 0) { 
        execl("/usr/bin/unzip", "unzip", "players.zip", "-d", "players", NULL);

    } else if (pid_unzip > 0) { 
        waitpid(pid_unzip, &ext_status, 0);

        if (WIFEXITED(ext_status) && WEXITSTATUS(ext_status) == 0) {
            printf("Berhasil di unzip\n");
        } else {
            perror("Gagal unzip");
            exit(1);
        }
    }
}


void hapusFile() {
pid_t pid_hps = fork();
int status_hps;

if (pid_hps == 0) { 
    execl("/bin/rm", "rm", "players.zip", NULL);
    exit(1);
    
}else if (pid_hps > 0) { 
    waitpid(pid_hps, &status_hps, 0);

        if (WIFEXITED(status_hps) && WEXITSTATUS(status_hps) == 0) {
            printf("Berhasil hapus 'players,zip'\n");
        } else {
            perror("Gagal hapus 'players.zip'");
            exit(1);
        }
    }
}

void hapusNonCav() {
    pid_t delete_pid = fork();
    int delete_status;

    if (delete_pid == 0) { 
        execl("/usr/bin/find", "find",  "players", "-type", "f", "!", "-name", "*Cavaliers*", "-exec", "rm", "{}", "+", NULL);
    } else if (delete_pid > 0) { 
        waitpid(delete_pid, &delete_status, 0);

        if (WIFEXITED(delete_status) && WEXITSTATUS(delete_status) == 0) {
            printf("Bukan Caveliars berhasil dihapus.\n");
        } else {
            perror("Gagal menghapus yang bukan Caveliers");
            exit(1);
        }
    }
}

void pindahPosisi(char *position, char *destination) {
    char regex[100];
    snprintf(regex, sizeof(regex), ".*-%s-.*\\.png", position);

    char *args[] = {"find", ".", "-type", "f", "-regex", regex, "-exec", "mv", "{}", destination, ";", NULL};
    execv("/usr/bin/find", args);

    perror("Gagal menjalankan find");
    exit(1);
}

void CreateMovePlayers() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    int status;

    for (int i = 0; i < num_positions; i++) {
        pid_t mkdir_pid = fork();

        if (mkdir_pid < 0) {
            exit(1);

        } else if (mkdir_pid == 0) {
            char path[50];
            snprintf(path, sizeof(path), "players/%s", positions[i]);

            char *args[] = {"mkdir", "-p", path, NULL};
            execv("/usr/bin/mkdir", args);
            exit(1);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }

    for (int i = 0; i < num_positions; i++) {
        pid_t find_pid = fork();

        if (find_pid < 0) {
            exit(1);

        } else if (find_pid == 0) {
            char destination[50];
            snprintf(destination, sizeof(destination), "players/%s/", positions[i]);
            pindahPosisi(positions[i], destination);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }
}

void output() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    
    FILE *file = fopen("Formasi.txt", "w");
    
    if (file == NULL) {
        perror("Gagal membuka/membuat file Formasi.txt");
        return;
    }
    
    for (int i = 0; i < num_positions; i++) {
        char path[50];
        snprintf(path, sizeof(path), "players/%s", positions[i]);
        int player_count = 0;
        
        DIR *dir = opendir(path);
        if (dir) {
            struct dirent *entry;
            while ((entry = readdir(dir))) {
                if (entry->d_type == DT_REG) {
                    player_count++;
                }
            }
            closedir(dir);
        }
        
        fprintf(file, "%s: %d\n", positions[i], player_count);
    }
    
    fclose(file);
}

void moveKyrie() {
    pid_t move_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    move_pid = fork();

    if (move_pid < 0) {
        exit(1);

    } else if (move_pid == 0) {\
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/PG/Cavaliers-PG-Kyrie-Irving.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(move_pid, &status, 0);
    }
}

void moveLeBron() {
    pid_t child_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    child_pid = fork();

    if (child_pid < 0) {
        exit(1);

    } else if (child_pid == 0) {
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/SF/Cavaliers-SF-LeBron-James.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(child_pid, &status, 0);
    }
}


int main() {
    buatFolder();
    downloadFile();
    unzipFile();
    hapusFile();
    hapusNonCav();
    CreateMovePlayers();
    output();
    moveKyrie();
    moveLeBron();

    return 0;
}