#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>

int cariFileSUSPICIOUS(const char *path) {
    FILE *file = fopen(path, "r");
    if (file) {
        char buffer[1024];
        while (fgets(buffer, sizeof(buffer), file)) {
            if (strstr(buffer, "SUSPICIOUS") != NULL) {
                fclose(file);
                return 1;
            }
        }
        fclose(file);
    }
    return 0;
}

void hapusFiledanAddlog(const char *path, const char *logPath) {
    if (cariFileSUSPICIOUS(path)) {
        struct stat st;
        if (stat(path, &st) == 0) {
            if (S_ISREG(st.st_mode)) {
                if (remove(path) == 0) {
                    time_t t = time(NULL);
                    struct tm *tm_info = localtime(&t);
                    char timestamp[20];
                    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

                    FILE *logFile = fopen(logPath, "a");
                    if (logFile) {
                        fprintf(logFile, "[%s] '%s' telah berhasil dihapus.\n", timestamp, path);
                        fclose(logFile);
                    } else {
                        perror("Gagal membuka file log");
                    }
                } else {
                    perror("Gagal menghapus file");
                }
            }
        }
    }
}

void cekDirektori(const char *dirPath, const char *logPath) {
    while (1) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir(dirPath);

        if (dir == NULL) {
            perror("Gagal membuka direktori");
            return;
        }

        while ((entry = readdir(dir))) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            if (entry->d_type == DT_REG) {
                hapusFiledanAddlog(filePath, logPath);
            }
        }

        closedir(dir);

        sleep(30);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Penggunaan: %s <path>\n", argv[0]);
        return 1;
    }

    const char *dirPath = argv[1];

    if (access(dirPath, F_OK) == -1) {
        perror("Direktori tidak ditemukan");
        return 1;
    }

    const char *homeDir = getenv("HOME");
    if (homeDir == NULL) {
        perror("Tidak dapat mengambil direktori home user");
        return 1;
    }

    char logPath[512];
    snprintf(logPath, sizeof(logPath), "%s/cleaner.log", homeDir);

    pid_t pid = fork();

    if (pid < 0) {
        perror("Gagal membuat proses anak");
        return 1;
    }

    if (pid > 0) {
        exit(0);
    }

    umask(0);

    int logFile = open(logPath, O_WRONLY | O_CREAT | O_APPEND, 0644);
    if (logFile == -1) {
        perror("Gagal membuka file log");
        return 1;
    }

    close(STDIN_FILENO);

    dup2(logFile, STDOUT_FILENO);
    dup2(logFile, STDERR_FILENO);

    cekDirektori(dirPath, logPath);

    return 0;
}
