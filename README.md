| Nama | NRP |
| ------ | ------ |
|Rahmad Aji Wicaksono       |5027221034        |
|Alma Amira Dewani          |5027221054        |
|Zidny Ilman Na'fian        |5027221072        |


# Soal 1
John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana.

## Cara Pengerjaan

**A.** Program tersebut menerima input path dengan menggunakan “argv”.

1. Membuat folder cleaner di terminal
```c
mkdir cleaner
```

2. Masuk ke folder cleaner di terminal
```c
cd cleaner
```

3. Membuat file cleaner.c di terminal
```c
touch cleaner.C
```

4. Membuka VSCode melalui terminal
```c
Untuk Ubuntu linux:
code ..

Untuk Kali linux:
code-oss
```

5. Membuat agar program dapat menerima input path
```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Penggunaan: %s <path>\n", argv[0]);
        return 1;
    }

    const char *dirPath = argv[1];

    return 0;
}
```

6. Mengecek apakah direktori input ada atau tidak
```c
int main(int argc, char *argv[]) {
    // kode sebelumnya

    if (access(dirPath, F_OK) == -1) {
        perror("Direktori tidak ditemukan");
        return 1;
    }
}
```

**B.** Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.

1. Menambahkan library pada program
```c
// library sebelumnya
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
```

2. Membuat program agar bisa mencari file dengan isi SUSPICIOUS
```c
// library

int cariFileSUSPICIOUS(const char *path) {
    FILE *file = fopen(path, "r");
    if (file) {
        char buffer[1024];
        while (fgets(buffer, sizeof(buffer), file)) {
            if (strstr(buffer, "SUSPICIOUS") != NULL) {
                fclose(file);
                return 1;
            }
        }
        fclose(file);
    }
    return 0;
}

// kode selanjutnya
```

3. Membuat program agar dapat menghapus file SUSPICIOUS
```c
// kode sebelumnya

void hapusFile(const char *path) {
    if (cariFileSUSPICIOUS(path)) {
        struct stat st;
        if (stat(path, &st) == 0) {
            if (S_ISREG(st.st_mode)) {
                if (remove(path) != 0) {
                    perror("Gagal menghapus file");
                }
            }
        }
    }
}

// kode selanjutnya
```

4. Membuat program agar dapat mengecek direktori secara terus menerus
```c
// kode sebelumnya

void cekDirektori(const char *dirPath) {
    while(1){
        DIR *dir;
        struct dirent *entry;

        dir = opendir(dirPath);

        if (dir == NULL) {
            perror("Gagal membuka direktori");
            return;
        }

        while ((entry = readdir(dir))) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            if (entry->d_type == DT_REG) {
                hapusFile(filePath);
            }
        }

        closedir(dir);
    }
}

// kode selanjutnya
```

**C.** Program tersebut harus berjalan secara daemon.

1. Membuat program agar menjadi daemon
```c
int main(int argc, char *argv[]) {
    // kode sebelumnya

    pid_t pid = fork();

    if (pid < 0) {
        perror("Gagal membuat proses anak");
        return 1;
    }

    if (pid > 0) {
        exit(0);
    }

    return 0;
}
```

2. Membuat program agar tidak terhubung dengan terminal
```c
int main(int argc, char *argv[]) {
    // kode sebelumnya

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    return 0
}
```

**D.** Program tersebut akan terus berjalan di background dengan jeda 30 detik.

1. Ubah fungsi cekDirektori agar program berjalan dengan jeda 30 detik
```c
// kode sebelumnya

void cekDirektori(const char *dirPath) {
    while(1){
        // kode sebelumnya

        sleep(30);
    }
}

// kode selanjutnya
```

**E.** Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.

Program ini tidak menggunakan fungsi 'system()' sama sekali.

**F.** Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

1. Ubah fungsi 'hapusFile()' menjadi fungsi 'hapusFiledanAddlog' seperti berikut
```c
void hapusFiledanAddlog(const char *path, const char *logPath) {
    if (cariFileSUSPICIOUS(path)) {
        struct stat st;
        if (stat(path, &st) == 0) {
            if (S_ISREG(st.st_mode)) {
                if (remove(path) == 0) {
                    time_t t = time(NULL);
                    struct tm *tm_info = localtime(&t);
                    char timestamp[20];
                    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

                    FILE *logFile = fopen(logPath, "a");
                    if (logFile) {
                        fprintf(logFile, "[%s] '%s' telah berhasil dihapus.\n", timestamp, path);
                        fclose(logFile);
                    } else {
                        perror("Gagal membuka file log");
                    }
                } else {
                    perror("Gagal menghapus file");
                }
            }
        }
    }
}
```

2. Tambahkan beberapa hal dalam fungsi 'cekDirektori()'
```c
// tambahkan logpath
void cekDirektori(const char *dirPath, const char *logPath) {
    while(1) {
        // kode sebelumnya

        while ((entry = readdir(dir))) {
            // kode sebelumnya

            // ubah pemanggilan fungsi
            if (entry->d_type == DT_REG) {
                hapusFiledanAddlog(filePath, logPath);
            }
        }

        // kode selanjutnya
    }
}
```

3. Tambahkan beberapa hal berikut pada fungsi 'main()'
```c
int main(int argc, char *argv[]) {
    // kode sebelumnya

    const char *homeDir = getenv("HOME");
    if (homeDir == NULL) {
        perror("Tidak dapat mengambil direktori home user");
        return 1;
    }

    char logPath[512];
    snprintf(logPath, sizeof(logPath), "%s/cleaner.log", homeDir);

    // kode sebelumnya

    umask(0);

    int logFile = open(logPath, O_WRONLY | O_CREAT | O_APPEND, 0644);
    if (logFile == -1) {
        perror("Gagal membuka file log");
        return 1;
    }

    // kode selanjutnya

    cekDirektori(dirPath, logPath);

    return 0;
}
```

- **Hasil output program**<br>
![hasil program ](gambar/output_soal1.png)
<br>

- **Kode full program**<br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>

int cariFileSUSPICIOUS(const char *path) {
    FILE *file = fopen(path, "r");
    if (file) {
        char buffer[1024];
        while (fgets(buffer, sizeof(buffer), file)) {
            if (strstr(buffer, "SUSPICIOUS") != NULL) {
                fclose(file);
                return 1;
            }
        }
        fclose(file);
    }
    return 0;
}

void hapusFiledanAddlog(const char *path, const char *logPath) {
    if (cariFileSUSPICIOUS(path)) {
        struct stat st;
        if (stat(path, &st) == 0) {
            if (S_ISREG(st.st_mode)) {
                if (remove(path) == 0) {
                    time_t t = time(NULL);
                    struct tm *tm_info = localtime(&t);
                    char timestamp[20];
                    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

                    FILE *logFile = fopen(logPath, "a");
                    if (logFile) {
                        fprintf(logFile, "[%s] '%s' telah berhasil dihapus.\n", timestamp, path);
                        fclose(logFile);
                    } else {
                        perror("Gagal membuka file log");
                    }
                } else {
                    perror("Gagal menghapus file");
                }
            }
        }
    }
}

void cekDirektori(const char *dirPath, const char *logPath) {
    while (1) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir(dirPath);

        if (dir == NULL) {
            perror("Gagal membuka direktori");
            return;
        }

        while ((entry = readdir(dir))) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            if (entry->d_type == DT_REG) {
                hapusFiledanAddlog(filePath, logPath);
            }
        }

        closedir(dir);

        sleep(30);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Penggunaan: %s <path>\n", argv[0]);
        return 1;
    }

    const char *dirPath = argv[1];

    if (access(dirPath, F_OK) == -1) {
        perror("Direktori tidak ditemukan");
        return 1;
    }

    const char *homeDir = getenv("HOME");
    if (homeDir == NULL) {
        perror("Tidak dapat mengambil direktori home user");
        return 1;
    }

    char logPath[512];
    snprintf(logPath, sizeof(logPath), "%s/cleaner.log", homeDir);

    pid_t pid = fork();

    if (pid < 0) {
        perror("Gagal membuat proses anak");
        return 1;
    }

    if (pid > 0) {
        exit(0);
    }

    umask(0);

    int logFile = open(logPath, O_WRONLY | O_CREAT | O_APPEND, 0644);
    if (logFile == -1) {
        perror("Gagal membuka file log");
        return 1;
    }

    close(STDIN_FILENO);

    dup2(logFile, STDOUT_FILENO);
    dup2(logFile, STDERR_FILENO);

    cekDirektori(dirPath, logPath);

    return 0;
}
```

## Kendala
Kendala dalam penulisan log.


# Soal 2
QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut.


## Cara Pengerjaan


### POINT A


Membuat program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.


1. Membuat folder cavs.c di terminal
```c
mkdir cavs.c
```

2. Membuka folder cavs.c dengan vscode
```c
code cavs.c
```

3. Membuat folder 'players'

```c
void buatFolder() {
    int ret = mkdir("players", 0755);

    if (ret == 0) {
        printf("Folder 'players' telah berhasil dibuat.\n");
    } else {
        perror("Gagal membuat folder 'players'");
        exit(1);
    }
}

```

**Penjelasan kode**
- `void buatFolder()` : Ini adalah deklarasi fungsi buatFolder
- `int ret = mkdir("players", 0755);` : fungsi mkdir digunakan untuk membuat direktori baru dengan nama "players". Angka 0755 adalah mode izin untuk direktori yang baru dibuat, yang menentukan izin akses untuk direktori tersebut. Fungsi ini mengembalikan 0 jika pembuatan direktori berhasil, dan -1 jika terjadi kesalahan.
- `if (ret == 0)`  : Ini adalah awal dari blok pernyataan kondisional. Jika nilai yang dikembalikan oleh mkdir adalah 0, yang menunjukkan bahwa pembuatan direktori berhasil.
- `printf("Folder 'players' telah berhasil dibuat.\n");` : Pernyataan ini akan mencetak pesan ke konsol yang memberi tahu bahwa direktori "players" telah berhasil dibuat.
- `else {` : Bagian ini menandai awal dari blok alternatif dalam pernyataan kondisional. Jika nilai yang dikembalikan oleh mkdir bukan 0, yang menunjukkan bahwa pembuatan direktori gagal.
- `perror("Gagal membuat folder 'players'");` : Pernyataan perror akan mencetak pesan kesalahan yang spesifik terkait dengan kesalahan yang terjadi selama pembuatan direktori.
- `exit(1);` : Fungsi exit digunakan untuk menghentikan program secara paksa dengan menetapkan kode status 1, yang menandakan adanya kesalahan.
<br>


**Output**


![output 1 ](gambar/2folder-player.png)
<br>


### POINT B


Program akan **mengunduh** file yang berisikan [database para pemain bola](https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK/view). Kemudian dalam program yang sama diminta dapat melakukan **extract “players.zip”** ke dalam folder players yang telah dibuat. Lalu **hapus file zip** tersebut agar tidak memenuhi komputer QQ.

1. Mengunduh file yang berisikan [database para pemain bola](https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK/view).
```c
void downloadFile() {
    pid_t pid_donlod= fork();
    int status;

    if (pid_donlod == 0) { 
        execl("/usr/bin/wget", "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL);

    } else if (pid_donlod > 0) { 
        waitpid(pid_donlod, &status, 0);
    }
}
```
**Penjelasan kode:**
- `pid_t pid_donlod= fork();` : Baris ini mendeklarasikan variabel pid_donlod yang bertipe pid_t dan memanggil fungsi fork(). fork() digunakan untuk membuat proses baru yang merupakan salinan dari proses pemanggil.
- `int status;` : Ini adalah deklarasi variabel status yang bertipe int. Variabel ini akan digunakan untuk menyimpan status dari proses anak.
- `if (pid_donlod == 0) {` : Ini adalah blok kondisional yang akan dieksekusi oleh proses anak. Jika pid_donlod sama dengan 0, artinya proses saat ini adalah proses anak.
- `execl("/usr/bin/wget", "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL);` : Fungsi execl digunakan untuk mengeksekusi perintah wget dengan argumen yang diberikan. Ini akan mengunduh file dari URL yang diberikan dan menyimpannya dengan nama "players.zip" di direktori saat ini.
- `} else if (pid_donlod > 0) {` : Jika pid_donlod lebih besar dari 0, itu menunjukkan bahwa ini adalah proses induk.
- `waitpid(pid_donlod, &status, 0);` : Fungsi waitpid akan menunggu proses anak selesai dieksekusi. Ini akan mengembalikan status dari proses anak, yang kemudian akan disimpan dalam variabel status.
<br>

**Output**


![output 2 ](gambar/2download-zip.png)

<br>

2. Melakukan unzip ke dalam folder players yang telah dibuat.
```c
void unzipFile() {
    pid_t pid_unzip = fork();
    int ext_status;

    if (pid_unzip == 0) { 
        execl("/usr/bin/unzip", "unzip", "players.zip", "-d", "players", NULL);

    } else if (pid_unzip > 0) { 
        waitpid(pid_unzip, &ext_status, 0);

        if (WIFEXITED(ext_status) && WEXITSTATUS(ext_status) == 0) {
            printf("Berhasil di unzip\n");
        } else {
            perror("Gagal unzip");
            exit(1);
        }
    }
}
```
**Penjelasan kode**
- `pid_t pid_unzip = fork();` : Baris ini mendeklarasikan variabel pid_unzip yang bertipe pid_t dan memanggil fungsi fork(). Ini akan menciptakan proses baru yang merupakan salinan dari proses pemanggil.
- `int ext_status;` : Ini adalah deklarasi variabel ext_status yang bertipe int. Variabel ini akan digunakan untuk menyimpan status eksekusi dari proses anak.
- `if (pid_unzip == 0) {` : Ini adalah blok kondisional yang akan dieksekusi oleh proses anak. Jika pid_unzip sama dengan 0, artinya proses saat ini adalah proses anak.
- `execl("/usr/bin/unzip", "unzip", "players.zip", "-d", "players", NULL);` : Fungsi execl digunakan untuk mengeksekusi perintah unzip dengan argumen yang diberikan. Ini akan mengekstrak file "players.zip" ke dalam direktori "players".
- `} else if (pid_unzip > 0) {` : Jika pid_unzip lebih besar dari 0, itu menunjukkan bahwa ini adalah proses induk.
- `waitpid(pid_unzip, &ext_status, 0);` : Fungsi waitpid akan menunggu proses anak selesai dieksekusi. Ini akan mengembalikan status eksekusi dari proses anak, yang kemudian akan disimpan dalam variabel ext_status.
- `if (WIFEXITED(ext_status) && WEXITSTATUS(ext_status) == 0) {` : Pernyataan ini memeriksa apakah proses anak keluar secara normal dan apakah kode keluarannya adalah 0, yang menunjukkan bahwa ekstraksi berhasil.
- `printf("Berhasil di unzip\n");` : Jika ekstraksi berhasil, pesan keberhasilan akan dicetak.
- `} else {` : Jika ekstraksi gagal, blok pernyataan alternatif akan dieksekusi.
- `perror("Gagal unzip");` : Pernyataan perror akan mencetak pesan kesalahan spesifik terkait dengan kegagalan ekstraksi.
- `exit(1);` : Fungsi exit digunakan untuk menghentikan program secara paksa dengan menetapkan kode status 1, yang menandakan adanya kesalahan.

<br>

**Output**

![output 3 ](gambar/2unzip.png)

<br>

3. Menghapus file zip
```c
void hapusFile() {
pid_t pid_hps = fork();
int status_hps;

if (pid_hps == 0) { 
    execl("/bin/rm", "rm", "players.zip", NULL);
    exit(1);
    
}else if (pid_hps > 0) { 
    waitpid(pid_hps, &status_hps, 0);

        if (WIFEXITED(status_hps) && WEXITSTATUS(status_hps) == 0) {
            printf("Berhasil hapus 'players,zip'\n");
        } else {
            perror("Gagal hapus 'players.zip'");
            exit(1);
        }
    }
}

```
**Penjelasan kode**
- `pid_t pid_hps = fork();` : Baris ini mendeklarasikan variabel pid_hps yang bertipe pid_t dan memanggil fungsi fork(). Ini akan menciptakan proses baru yang merupakan salinan dari proses pemanggil.
- `int status_hps;` : Ini adalah deklarasi variabel status_hps yang bertipe int. Variabel ini akan digunakan untuk menyimpan status eksekusi dari proses anak.
- `if (pid_hps == 0) {` : Ini adalah blok kondisional yang akan dieksekusi oleh proses anak. Jika pid_hps sama dengan 0, artinya proses saat ini adalah proses anak.
- `execl("/bin/rm", "rm", "players.zip", NULL);` : Fungsi execl digunakan untuk mengeksekusi perintah rm dengan argumen yang diberikan. Ini akan menghapus file 'players.zip'.
- `exit(1);` : Jika perintah execl gagal dieksekusi, anak akan keluar dengan kode status 1.
- `} else if (pid_hps > 0) {` : Jika pid_hps lebih besar dari 0, itu menunjukkan bahwa ini adalah proses induk.
- `waitpid(pid_hps, &status_hps, 0);` : Fungsi waitpid akan menunggu proses anak selesai dieksekusi. Ini akan mengembalikan status eksekusi dari proses anak, yang kemudian akan disimpan dalam variabel status_hps.
- `if (WIFEXITED(status_hps) && WEXITSTATUS(status_hps) == 0) {` : Pernyataan ini memeriksa apakah proses anak keluar secara normal dan apakah kode keluarannya adalah 0, yang menunjukkan bahwa penghapusan berhasil.
- `printf("Berhasil hapus 'players,zip'\n");` : Jika penghapusan berhasil, pesan keberhasilan akan dicetak.
- `} else {` : Jika penghapusan gagal, blok pernyataan alternatif akan dieksekusi.
- `perror("Gagal hapus 'players.zip'");` : Pernyataan perror akan mencetak pesan kesalahan spesifik terkait dengan kegagalan penghapusan.
- `exit(1);` : Fungsi exit digunakan untuk menghentikan program secara paksa dengan menetapkan kode status 1, yang menandakan adanya kesalahan.

<br>

**Output**


![output 4 ](gambar/2hapus-zip.png)

<br>

### POINT C


Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.


**Code:**
```c
void hapusNonCav() {
    pid_t delete_pid = fork();
    int delete_status;

    if (delete_pid == 0) { 
        execl("/usr/bin/find", "find",  "players", "-type", "f", "!", "-name", "*Cavaliers*", "-exec", "rm", "{}", "+", NULL);
    } else if (delete_pid > 0) { 
        waitpid(delete_pid, &delete_status, 0);

        if (WIFEXITED(delete_status) && WEXITSTATUS(delete_status) == 0) {
            printf("Bukan Caveliars berhasil dihapus.\n");
        } else {
            perror("Gagal menghapus yang bukan Caveliers");
            exit(1);
        }
    }
}
```
**Penjelasan kode**
- `pid_t delete_pid = fork();` : Baris ini mendeklarasikan variabel delete_pid yang bertipe pid_t dan memanggil fungsi fork(). Ini akan menciptakan proses baru yang merupakan salinan dari proses pemanggil.
- `int delete_status;` : Ini adalah deklarasi variabel delete_status yang bertipe int. Variabel ini akan digunakan untuk menyimpan status eksekusi dari proses anak.
- `if (delete_pid == 0) {` : Ini adalah blok kondisional yang akan dieksekusi oleh proses anak. Jika delete_pid sama dengan 0, artinya proses saat ini adalah proses anak.
- `execl("/usr/bin/find", "find", "players", "-type", "f", "!", "-name", "*Cavaliers*", "-exec", "rm", "{}", "+", NULL);` : Fungsi execl digunakan untuk mengeksekusi perintah find dengan argumen yang diberikan. Perintah ini akan mencari file dalam direktori 'players' yang bukan merupakan bagian dari tim Cavaliers, dan kemudian menghapusnya.
- `else if (delete_pid > 0) {` : Jika delete_pid lebih besar dari 0, itu menunjukkan bahwa ini adalah proses induk.
- `waitpid(delete_pid, &delete_status, 0);` : Fungsi waitpid akan menunggu proses anak selesai dieksekusi. Ini akan mengembalikan status eksekusi dari proses anak, yang kemudian akan disimpan dalam variabel delete_status.
- `if (WIFEXITED(delete_status) && WEXITSTATUS(delete_status) == 0) {` : Pernyataan ini memeriksa apakah proses anak keluar secara normal dan apakah kode keluarannya adalah 0, yang menunjukkan bahwa penghapusan berhasil.
- `printf("Bukan Cavaliers berhasil dihapus.\n");` : Jika penghapusan berhasil, pesan keberhasilan akan dicetak.
- `} else {` : Jika penghapusan gagal, blok pernyataan alternatif akan dieksekusi.
- `perror("Gagal menghapus yang bukan Cavaliers");` : Pernyataan perror akan mencetak pesan kesalahan spesifik terkait dengan kegagalan penghapusan.
- `exit(1);` : Fungsi exit digunakan untuk menghentikan program secara paksa dengan menetapkan kode status 1, yang menandakan adanya kesalahan.

<br>

**Output**


![output 5 ](gambar/2hapus-noncav.png)

<br>

### POINT D

Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).


**Code**
```c
void pindahPosisi(char *position, char *destination) {
    char regex[100];
    snprintf(regex, sizeof(regex), ".*-%s-.*\\.png", position);

    char *args[] = {"find", ".", "-type", "f", "-regex", regex, "-exec", "mv", "{}", destination, ";", NULL};
    execv("/usr/bin/find", args);

    perror("Gagal menjalankan find");
    exit(1);
}
```

```c
void CreateMovePlayers() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    int status;

    for (int i = 0; i < num_positions; i++) {
        pid_t mkdir_pid = fork();

        if (mkdir_pid < 0) {
            exit(1);

        } else if (mkdir_pid == 0) {
            char path[50];
            snprintf(path, sizeof(path), "players/%s", positions[i]);

            char *args[] = {"mkdir", "-p", path, NULL};
            execv("/usr/bin/mkdir", args);
            exit(1);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }

    for (int i = 0; i < num_positions; i++) {
        pid_t find_pid = fork();

        if (find_pid < 0) {
            exit(1);

        } else if (find_pid == 0) {
            char destination[50];
            snprintf(destination, sizeof(destination), "players/%s/", positions[i]);
            pindahPosisi(positions[i], destination);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }
}
```
**Penjelasan kode**

Fungsi pindahPosisi:
- `void pindahPosisi(char *position, char *destination) {` : Deklarasi fungsi pindahPosisi yang mengambil dua argumen, yaitu position yang merupakan posisi pemain dan destination yang merupakan direktori tujuan.
- `char regex[100];` : Deklarasi array karakter regex dengan ukuran 100.
- `snprintf(regex, sizeof(regex), ".*-%s-.*\\.png", position);` : Mengisi variabel regex dengan pola regex yang mencocokkan file-file gambar pemain yang sesuai dengan posisi yang diberikan.
- `char *args[] = {"find", ".", "-type", "f", "-regex", regex, "-exec", "mv", "{}", destination, ";", NULL}; :` Mendefinisikan array argumen yang akan digunakan untuk menjalankan perintah find dan mv dengan menggunakan pola regex yang telah dibuat sebelumnya.
- `execv("/usr/bin/find", args);` : Menjalankan perintah find dengan menggunakan argumen yang telah didefinisikan sebelumnya.
- `perror("Gagal menjalankan find");` : Mencetak pesan kesalahan jika perintah find gagal dieksekusi.
- `exit(1);` : Menghentikan program dengan kode status 1 jika terjadi kesalahan.

<br>

Fungsi CreateMovePlayers:

- `void CreateMovePlayers() {` : Deklarasi fungsi CreateMovePlayers.
- `char *positions[] = {"PG", "SG", "PF", "SF", "C"};` : Mendefinisikan array positions yang berisi posisi-posisi pemain basket.
- `int num_positions = sizeof(positions) / sizeof(positions[0]);` : Menghitung jumlah posisi pemain dalam array positions.
- `int status;` : Deklarasi variabel status yang akan digunakan untuk menyimpan status dari proses.
- Loop pertama berfungsi untuk membuat direktori untuk setiap posisi pemain yang tercantum dalam array positions menggunakan perintah mkdir.
- Loop kedua berfungsi untuk memindahkan file-file pemain ke direktori yang sesuai dengan posisi mereka menggunakan fungsi pindahPosisi yang telah didefinisikan sebelumnya.

<br>


**Output**


![output 6 ](gambar/2kategori.png)

<br>

### POINT E


Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
PG: {jumlah pemain}
SG: {jumlah pemain}
SF: {jumlah pemain}
PF: {jumlah pemain}
C: {jumlah pemain}


```c
void output() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    
    FILE *file = fopen("Formasi.txt", "w");
    
    if (file == NULL) {
        perror("Gagal membuka/membuat file Formasi.txt");
        return;
    }
    
    for (int i = 0; i < num_positions; i++) {
        char path[50];
        snprintf(path, sizeof(path), "players/%s", positions[i]);
        int player_count = 0;
        
        DIR *dir = opendir(path);
        if (dir) {
            struct dirent *entry;
            while ((entry = readdir(dir))) {
                if (entry->d_type == DT_REG) {
                    player_count++;
                }
            }
            closedir(dir);
        }
        
        fprintf(file, "%s: %d\n", positions[i], player_count);
    }
    
    fclose(file);
}
```

**Penjelasan kode**
- void output() { : Deklarasi fungsi output.
- `char *positions[] = {"PG", "SG", "PF", "SF", "C"};` : Mendefinisikan array positions yang berisi posisi-posisi pemain basket.
- `int num_positions = sizeof(positions) / sizeof(positions[0]);` : Menghitung jumlah posisi pemain dalam array positions.
- `FILE *file = fopen("Formasi.txt", "w");` : Membuka file Formasi.txt dalam mode penulisan (write).
- `if (file == NULL) {` : Memeriksa apakah file berhasil dibuka. Jika gagal, maka akan dicetak pesan kesalahan.
- Loop for berfungsi untuk menelusuri setiap posisi pemain dalam array positions.
- `char path[50];` : Deklarasi array karakter path dengan panjang 50.
- `snprintf(path, sizeof(path), "players/%s", positions[i]);` : Mengisi variabel path dengan direktori pemain berdasarkan posisi yang sedang diperiksa.
- `int player_count = 0;` : Mendeklarasikan variabel player_count yang akan menyimpan jumlah pemain dalam posisi yang sedang diperiksa.
- `DIR *dir = opendir(path);` : Membuka direktori yang ditentukan oleh variabel path.
- Blok `if` akan dieksekusi jika direktori berhasil dibuka. Di dalam blok `if`, looping `while` akan menelusuri setiap entri (file) dalam direktori.
- Pada looping while,  `entry->d_type == DT_REG` digunakan untuk memeriksa apakah entri yang sedang diperiksa adalah file reguler (bukan direktori).
- Jika entri yang diperiksa adalah file reguler, maka `player_count` akan bertambah satu.
- Setelah selesai menelusuri direktori, `closedir(dir)` akan menutup direktori.
- `fprintf(file, "%s: %d\n", positions[i], player_count);` : Menulis hasil kategorisasi posisi dan jumlah pemain ke dalam file Formasi.txt.
- `fclose(file);` : Menutup file setelah selesai menulis.

<br>

**Output**

![output 7 ](gambar/2output.png)

<br>

### POINT F


Membuat folder 'clutch' dan memasukan foto pemain yang menembakkan [game-winning shot](https://en.wikipedia.org/wiki/Kyrie_Irving) pada ajang NBA Finals 2016.


**code**
```c
void moveKyrie() {
    pid_t move_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    move_pid = fork();

    if (move_pid < 0) {
        exit(1);

    } else if (move_pid == 0) {\
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/PG/Cavaliers-PG-Kyrie-Irving.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(move_pid, &status, 0);
    }
}
```

**Penjelasan kode:**
- `pid_t move_pid;` : Deklarasi variabel move_pid yang bertipe pid_t. Variabel ini akan digunakan untuk menyimpan ID proses.
- `int status;` : Deklarasi variabel status yang akan digunakan untuk menyimpan status dari proses.
- `struct stat st;` : Deklarasi variabel st dari tipe struct stat yang akan digunakan untuk menyimpan informasi tentang file.
- `if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {` : Pemeriksaan apakah folder "clutch" sudah ada. Jika tidak, maka folder akan dibuat menggunakan perintah mkdir dengan hak akses 0755.
- `int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);` : Membuat folder "clutch" jika folder tersebut belum ada. Jika pembuatan folder gagal, maka akan dicetak pesan kesalahan.
- `move_pid = fork();` : Menciptakan proses anak dengan menggunakan fork() dan menyimpan ID proses anak ke dalam move_pid.
- `if (move_pid < 0) {` : Pemeriksaan apakah proses anak berhasil dibuat. Jika tidak, maka program akan berhenti.
- `else if (move_pid == 0) {` : Jika proses anak berhasil dibuat, maka proses anak akan mengeksekusi perintah mv untuk memindahkan file gambar pemain Kyrie Irving ke dalam folder "clutch".
- `char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/PG/Cavaliers-PG-Kyrie-Irving.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};` : Menyiapkan argumen yang akan digunakan untuk memindahkan file menggunakan perintah mv.
- `execvp("mv", args);` : Melakukan eksekusi perintah mv dengan menggunakan argumen yang telah disiapkan sebelumnya.
- `waitpid(move_pid, &status, 0);` : Proses induk akan menunggu proses anak selesai dieksekusi dengan menggunakan waitpid.

<br>

**Output**

![output 8 ](gambar/2winning-shot.png)

<br>

### Point G
Memajang foto pemain yang melakukan [The Block](https://en.wikipedia.org/wiki/The_Block_(basketball)) pada ajang yang sama, maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.


**Code**
```c
void moveLeBron() {
    pid_t child_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    child_pid = fork();

    if (child_pid < 0) {
        exit(1);

    } else if (child_pid == 0) {
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/SF/Cavaliers-SF-LeBron-James.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(child_pid, &status, 0);
    }
}
```

**Penjelasan kode**

sama seperti penjelasan di point F. bedanya adalah, di point ini memindahkan foto LeBron ke folder clutch.

- `pid_t child_pid;` : Deklarasi variabel child_pid yang bertipe pid_t. Variabel ini akan digunakan untuk menyimpan ID proses.
- `int status;` : Deklarasi variabel status yang akan digunakan untuk menyimpan status dari proses.
- `struct stat st;` : Deklarasi variabel st dari tipe struct stat yang akan digunakan untuk menyimpan informasi tentang file.
- `if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {` : Pemeriksaan apakah folder "clutch" sudah ada. Jika tidak, maka folder akan dibuat menggunakan perintah mkdir dengan hak akses 0755.
- `int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);` : Membuat folder "clutch" jika folder tersebut belum ada. Jika pembuatan folder gagal, maka akan dicetak pesan kesalahan.
- `child_pid = fork();` : Menciptakan proses anak dengan menggunakan fork() dan menyimpan ID proses anak ke dalam child_pid.
- `if (child_pid < 0) {` : Pemeriksaan apakah proses anak berhasil dibuat. Jika tidak, maka program akan berhenti.
- `else if (child_pid == 0) {` : Jika proses anak berhasil dibuat, maka proses anak akan mengeksekusi perintah mv untuk memindahkan file gambar pemain LeBron James ke dalam folder "clutch".
- `char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/SF/Cavaliers-SF-LeBron-James.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};` : Menyiapkan argumen yang akan digunakan untuk memindahkan file menggunakan perintah mv.
- `execvp("mv", args);` : Melakukan eksekusi perintah mv dengan menggunakan argumen yang telah disiapkan sebelumnya.
- `waitpid(move_pid, &status, 0);` : Proses induk akan menunggu proses anak selesai dieksekusi dengan menggunakan waitpid.

<br>

**Output**

![output 9 ](gambar/2the-block.png)

<br>

### Kode Lengkap

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <dirent.h>


void buatFolder() {
    int ret = mkdir("players", 0755);

    if (ret == 0) {
        printf("Folder 'players' telah berhasil dibuat.\n");
    } else {
        perror("Gagal membuat folder 'players'");
        exit(1);
    }
}
 
void downloadFile() {
    pid_t pid_donlod= fork();
    int status;

    if (pid_donlod == 0) { 
        execl("/usr/bin/wget", "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL);

    } else if (pid_donlod > 0) { 
        waitpid(pid_donlod, &status, 0);
    }
}

void unzipFile() {
    pid_t pid_unzip = fork();
    int ext_status;

    if (pid_unzip == 0) { 
        execl("/usr/bin/unzip", "unzip", "players.zip", "-d", "players", NULL);

    } else if (pid_unzip > 0) { 
        waitpid(pid_unzip, &ext_status, 0);

        if (WIFEXITED(ext_status) && WEXITSTATUS(ext_status) == 0) {
            printf("Berhasil di unzip\n");
        } else {
            perror("Gagal unzip");
            exit(1);
        }
    }
}


void hapusFile() {
pid_t pid_hps = fork();
int status_hps;

if (pid_hps == 0) { 
    execl("/bin/rm", "rm", "players.zip", NULL);
    exit(1);
    
}else if (pid_hps > 0) { 
    waitpid(pid_hps, &status_hps, 0);

        if (WIFEXITED(status_hps) && WEXITSTATUS(status_hps) == 0) {
            printf("Berhasil hapus 'players,zip'\n");
        } else {
            perror("Gagal hapus 'players.zip'");
            exit(1);
        }
    }
}

void hapusNonCav() {
    pid_t delete_pid = fork();
    int delete_status;

    if (delete_pid == 0) { 
        execl("/usr/bin/find", "find",  "players", "-type", "f", "!", "-name", "*Cavaliers*", "-exec", "rm", "{}", "+", NULL);
    } else if (delete_pid > 0) { 
        waitpid(delete_pid, &delete_status, 0);

        if (WIFEXITED(delete_status) && WEXITSTATUS(delete_status) == 0) {
            printf("Bukan Caveliars berhasil dihapus.\n");
        } else {
            perror("Gagal menghapus yang bukan Caveliers");
            exit(1);
        }
    }
}

void pindahPosisi(char *position, char *destination) {
    char regex[100];
    snprintf(regex, sizeof(regex), ".*-%s-.*\\.png", position);

    char *args[] = {"find", ".", "-type", "f", "-regex", regex, "-exec", "mv", "{}", destination, ";", NULL};
    execv("/usr/bin/find", args);

    perror("Gagal menjalankan find");
    exit(1);
}

void CreateMovePlayers() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    int status;

    for (int i = 0; i < num_positions; i++) {
        pid_t mkdir_pid = fork();

        if (mkdir_pid < 0) {
            exit(1);

        } else if (mkdir_pid == 0) {
            char path[50];
            snprintf(path, sizeof(path), "players/%s", positions[i]);

            char *args[] = {"mkdir", "-p", path, NULL};
            execv("/usr/bin/mkdir", args);
            exit(1);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }

    for (int i = 0; i < num_positions; i++) {
        pid_t find_pid = fork();

        if (find_pid < 0) {
            exit(1);

        } else if (find_pid == 0) {
            char destination[50];
            snprintf(destination, sizeof(destination), "players/%s/", positions[i]);
            pindahPosisi(positions[i], destination);

        } else {
            wait(&status);
            if (WEXITSTATUS(status) != 0) {
                exit(1);
            }
        }
    }
}

void output() {
    char *positions[] = {"PG", "SG", "PF", "SF", "C"};
    int num_positions = sizeof(positions) / sizeof(positions[0]);
    
    FILE *file = fopen("Formasi.txt", "w");
    
    if (file == NULL) {
        perror("Gagal membuka/membuat file Formasi.txt");
        return;
    }
    
    for (int i = 0; i < num_positions; i++) {
        char path[50];
        snprintf(path, sizeof(path), "players/%s", positions[i]);
        int player_count = 0;
        
        DIR *dir = opendir(path);
        if (dir) {
            struct dirent *entry;
            while ((entry = readdir(dir))) {
                if (entry->d_type == DT_REG) {
                    player_count++;
                }
            }
            closedir(dir);
        }
        
        fprintf(file, "%s: %d\n", positions[i], player_count);
    }
    
    fclose(file);
}

void moveKyrie() {
    pid_t move_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    move_pid = fork();

    if (move_pid < 0) {
        exit(1);

    } else if (move_pid == 0) {\
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/PG/Cavaliers-PG-Kyrie-Irving.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(move_pid, &status, 0);
    }
}

void moveLeBron() {
    pid_t child_pid;
    int status;

    struct stat st;
    if (stat("/home/baebluu/praktikum/sisop/modul2/no2/clutch", &st) == -1) {
        int ret = mkdir("/home/baebluu/praktikum/sisop/modul2/no2/clutch", 0755);
        if (ret != 0) {
            perror("Gagal membuat folder 'clutch'");
            exit(1);
        }
    }

    child_pid = fork();

    if (child_pid < 0) {
        exit(1);

    } else if (child_pid == 0) {
        char *args[] = {"mv", "/home/baebluu/praktikum/sisop/modul2/no2/players/SF/Cavaliers-SF-LeBron-James.png", "/home/baebluu/praktikum/sisop/modul2/no2/clutch", NULL};
        execvp("mv", args);

    } else {
        waitpid(child_pid, &status, 0);
    }
}


int main() {
    buatFolder();
    downloadFile();
    unzipFile();
    hapusFile();
    hapusNonCav();
    CreateMovePlayers();
    output();
    moveKyrie();
    moveLeBron();

    return 0;
}
```

## Kendala

- Kesuliatan dalam mengkategorikan pemain sesuai posisinya.
- kesulitan dalam memindahkan foto pemain sesuai dengan posisinya.

<br>

## Revisi

Revisi dilakukan dengan melengkapi dan menambahkan kode dari point D, E, F, dan G.

- merubah point F dan G. yang tadinya endownload gambar jadi memindah gambar

kode sebelum
```c
void tambahClutchPhotos() {
    char *clutch_players[] = {"Kyrie-Irving.jpg", "LeBron-James.jpg"};
    int num_clutch_players = sizeof(clutch_players) / sizeof(clutch_players[0]);

    char *clutch_urls[] = {
        "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Kyrie_Irving_-_51831772061_01_%28cropped%29.jpg/220px-Kyrie_Irving_-_51831772061_01_%28cropped%29.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/LeBron_James_%2815662939969%29.jpg/180px-LeBron_James_%2815662939969%29.jpg"
    };

    int ret = mkdir("clutch", 0755);
    if (ret == 0) {
        printf("Folder 'clutch' telah berhasil dibuat.\n");
    } else {
        perror("Gagal membuat folder 'clutch'");
        exit(1);
    }

    for (int i = 0; i < num_clutch_players; i++) {
        pid_t download_pid = fork();
        int status;

        if (download_pid == 0) {
            char filename[50];
            snprintf(filename, sizeof(filename), "clutch/%s", clutch_players[i]);
            execl("/usr/bin/wget", "wget", clutch_urls[i], "-O", filename, NULL);

        } else if (download_pid > 0) {
            waitpid(download_pid, &status, 0);
        }
    }
}
```

<br>

# Soal 3

# Langkah-langkah Pengerjaan

#### 1. Fungsi membuat folder khusus dengan nama foldernya berformat timestamp[YYYY-MM-dd_HH:mm:ss]. Folder akan terbuat per 30 detik.

Kode:
``` c
void membuatFolderTimestamp(char* timestamp) {
    time_t waktu;
    struct tm *info_tm;

    do {
        time(&waktu);
        info_tm = localtime(&waktu);
        strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", info_tm);

        if (mkdir(timestamp, 0777) == -1) {
            if (errno != EEXIST) {
                perror("Gagal membuat folder");
                exit(EXIT_FAILURE);
            }
        } else {
            printf("Folder %s sukses terbuat.\n", timestamp);
            break;
        }

        sleep(30);
    } while (1);
}
```

Penjelasan kode:

- `time_t waktu;`

**time_t** adalah tipe data untuk merepresentasikan waktu saat ini dan disimpan kedalam variabel **waktu**.

- `struct tm *info_tm;`

**struct tm** adalah tipe data yang digunakan untuk menyimpan informasi terkait waktu seperti tahun, bulan, hari, jam, dan sebagainya. **info_tm** adalah pointer yang akan digunakan untuk menunjuk ke struktur data ini.

- `time(&waktu);`

Fungsi **time** digunakan untuk mendapatkan waktu saat ini dalam bentuk bilangan bertipe **time_t**. Nilai waktu ini akan disimpan dalam variabel **waktu**.

- `info_tm = localtime(&waktu);`

Fungsi **localtime** digunakan untuk mengonversi waktu dalam bentuk **time_t** ke struktur **struct tm** yang memuat informasi terperinci tentang waktu (tahun, bulan, hari, jam, dll). Hasil konversi ini disimpan dalam variabel **info_tm**.

- `strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", info_tm);`

Fungsi **strftime** digunakan untuk memformat waktu dari **struct tm** ke dalam format string tertentu. Di sini, waktu akan diformat dalam format "YYYY-MM-DD_HH:MM:SS" dan disimpan dalam variabel **timestamp**.

- `if (mkdir(timestamp, 0777) == -1) {`

Fungsi **mkdir** digunakan untuk membuat direktori. Jika pembuatan folder gagal, maka fungsi ini akan mengembalikan nilai -1, menandakan terjadi error.

- `if (errno != EEXIST) {`

**errno** adalah variabel global yang menyimpan kode error terakhir. **EEXIST** adalah kode error yang menunjukkan bahwa direktori yang hendak dibuat sudah ada. Jika error yang terjadi bukan karena folder sudah ada (EEXIST), maka blok ini akan dieksekusi.

- `perror("Gagal membuat folder");`

Fungsi **perror** digunakan untuk mencetak pesan error ke layar berdasarkan kode error terakhir. Pesan yang dicetak adalah "Gagal membuat folder".

- `exit(EXIT_FAILURE);`

Fungsi **exit** digunakan untuk mengakhiri program. Argument **EXIT_FAILURE** menunjukkan bahwa program berakhir dengan status kegagalan.

- `printf("Folder %s sukses terbuat.\n", timestamp);`

Pesan ini akan mencetak bahwa folder telah berhasil dibuat dengan timestamp tertentu.

- `break;`

Statement break digunakan untuk keluar dari loop do-while.

- `sleep(30);`

Fungsi sleep akan menunda eksekusi program selama 30 detik sebelum memulai iterasi berikutnya dalam loop do-while.

- `} while (1);`

Akhir dari loop do-while. Karena kondisi di dalam kurung kurawal adalah 1 (yang selalu benar), maka loop akan terus berjalan hingga dihentikan oleh perintah break di dalam loop.

#### 2. Fungsi mendownload gambar, setiap folder akan diisi dengan 15 gambar yang diunduh dari https://source.unsplash.com/{widthxheight}. Setiap gambar diunduh setiap 5 detik. Ukuran gambar adalah (t%1000)+50 piksel, di mana t adalah detik Epoch Unix.

Kode:
``` c
void downloadGambar(char* timestamp, int i) {
    char url[100];
    char namaFile[100];
    time_t waktu = time(NULL);
    int ukuranGambar = (waktu % 1000) + 50;

    sprintf(url, "https://source.unsplash.com/%dx%d", ukuranGambar, ukuranGambar);
    sprintf(namaFile, "%s/image%d.jpg", timestamp, i);

    if (execlp("curl", "curl", "-s", "-o", namaFile, url, (char *)NULL) == -1) {
        perror("execlp");
        exit(EXIT_FAILURE);
    }
}
```

Penjelasan kode:
- `char url[100];`

**url** adalah array karakter dengan kapasitas untuk menyimpan 100 karakter. Variabel ini akan digunakan untuk menyimpan URL dari gambar yang akan didownload.

- `char namaFile[100]`;

**namaFile** adalah array karakter dengan kapasitas untuk menyimpan 100 karakter. Variabel ini akan digunakan untuk menyimpan nama file hasil download.

- `time_t waktu = time(NULL);`

**time_t** adalah tipe data untuk merepresentasikan waktu. Variabel **waktu** akan digunakan untuk menyimpan nilai waktu saat ini. Fungsi **time(NULL)** mengembalikan waktu saat ini dalam bentuk **time_t**.

- `int ukuranGambar = (waktu % 1000) + 50;`

Ini adalah proses untuk menghasilkan ukuran acak untuk gambar. **waktu % 1000** akan memberikan angka antara 0 hingga 999, lalu ditambahkan **50** untuk memastikan ukurannya selalu minimal **50**.

- `sprintf(url, "https://source.unsplash.com/%dx%d", ukuranGambar, ukuranGambar);`

**sprintf** digunakan untuk memformat string dan menyimpannya dalam variabel **url**. Di sini, URL gambar diambil dari situs **unsplash.com** dengan ukuran sesuai dengan nilai **ukuranGambar**.

- `sprintf(namaFile, "%s/image%d.jpg", timestamp, i);`

Menggunakan **sprintf** lagi untuk membentuk nama file. Nama file akan mengandung **timestamp** dan nomor gambar (i), serta ekstensi **.jpg**.

- `if (execlp("curl", "curl", "-s", "-o", namaFile, url, (char *)NULL) == -1) {`

**execlp** adalah fungsi yang digunakan untuk menjalankan program eksternal (dalam hal ini, perintah curl). Perintah **curl** digunakan untuk mengunduh file dari URL.

- `perror("execlp");`

Jika **execlp** mengembalikan nilai -1 (menunjukkan terjadi error), maka perror akan mencetak pesan error yang terkait.

- `exit(EXIT_FAILURE);`

Fungsi **exit** digunakan untuk mengakhiri program. Argument **EXIT_FAILURE** menunjukkan bahwa program berakhir dengan status kegagalan.

#### 3. Setelah sebuah folder telah terisi oleh 15 gambar, folder akan di-zip.

Kode:

``` c
void zipFolder(char* timestamp) {
    char command[100];
    sprintf(command, "zip -r %s.zip %s", timestamp, timestamp);
    pid_t pid = fork();

    if (pid == 0) {
        execlp("zip", "zip", "-r", strcat(timestamp, ".zip"), timestamp, NULL);
        perror("Gagal melakukan zip");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}

```

Penjelasan kode:
- `char command[100];`

**command** adalah array karakter dengan kapasitas untuk menyimpan 100 karakter. Variabel ini akan digunakan untuk menyimpan perintah zip yang akan dijalankan.

- `sprintf(command, "zip -r %s.zip %s", timestamp, timestamp);`

Menggunakan **sprintf** untuk membentuk perintah zip yang akan dijalankan. Perintah ini akan mengompres folder dengan nama **timestamp** ke dalam file **zip** dengan nama **timestamp.zip**.

- `pid_t pid = fork();`

Fungsi **fork** digunakan untuk membuat salinan proses yang berjalan. Hasil dari fork adalah 0 jika proses baru berhasil dibuat, dan nilai lainnya adalah ID dari proses anak.

- `if (pid == 0) {`

Blok ini akan dijalankan oleh proses anak.

- `execlp("zip", "zip", "-r", strcat(timestamp, ".zip"), timestamp, NULL);`

Fungsi **execlp** digunakan untuk menggantikan program yang sedang berjalan dengan program lain (dalam hal ini, perintah zip). Perintah ini akan mengompres folder dengan nama **timestamp** dan nama file zip akan diubah menjadi **timestamp.zip**.

- `perror("Gagal melakukan zip");`

Jika eksekusi perintah **zip** mengalami error, pesan error akan dicetak ke layar dengan menggunakan perror.

- `exit(EXIT_FAILURE);`

Fungsi **exit** digunakan untuk mengakhiri program. Argument **EXIT_FAILURE** menunjukkan bahwa program berakhir dengan status kegagalan.

- `wait(NULL);`

Fungsi **wait** digunakan untuk menunggu proses anak selesai. Ini memastikan bahwa proses induk menunggu hingga proses anak selesai sebelum melanjutkan eksekusi.

4. Setelah di-zip, folder akan dihapus sehingga hanya menyisakan file .zip dengan format nama [YYYY-MM-dd_HH:mm:ss].

Kode:
``` c
void deleteFolder(char* timestamp) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-r", timestamp, NULL);
        perror("Gagal menghapus folder");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}
```

Penjelasan kode:
- `pid_t pid = fork();`

Fungsi **fork** digunakan untuk membuat salinan proses yang berjalan. Hasil dari fork adalah 0 jika proses baru berhasil dibuat, dan nilai lainnya adalah ID dari proses anak.

- `if (pid == 0) {`

Blok ini akan dijalankan oleh proses anak.

- `execlp("rm", "rm", "-r", timestamp, NULL);`

Fungsi **execlp** digunakan untuk menggantikan program yang sedang berjalan dengan program lain (dalam hal ini, perintah rm). Perintah ini akan menghapus folder dengan nama **timestamp**.

- `perror("Gagal menghapus folder");`

Jika eksekusi perintah rm mengalami error, pesan error akan dicetak ke layar dengan menggunakan **perror**.

- `exit(EXIT_FAILURE);`

Fungsi **exit** digunakan untuk mengakhiri program. Argument **EXIT_FAILURE** menunjukkan bahwa program berakhir dengan status kegagalan.

- `wait(NULL);`

Fungsi **wait** digunakan untuk menunggu proses anak selesai. Ini memastikan bahwa proses induk menunggu hingga proses anak selesai sebelum melanjutkan eksekusi.

#### 5. Fungsi "killer",  sebuah program yang dapat membunuh proses dengan nama "lukisan". Program ini bisa dibuat dengan dua mode berbeda: mode 1 untuk membunuh semua proses dengan nama "lukisan" dan mode 2 untuk membunuh proses dengan pid tertentu

Kode:
``` c
void programKiller(int mode) {
    FILE *file = fopen("killer.c", "w");

    if (file != NULL) {
        fprintf(file, "#include <stdio.h>\n");
        fprintf(file, "#include <unistd.h>\n");
        fprintf(file, "#include <stdlib.h>\n");
        fprintf(file, "\n");
        fprintf(file, "int main() {\n");
        fprintf(file, "    pid_t child = fork();\n");
        fprintf(file, "    if (child == 0) {\n");
        if (mode == 1) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"lukisan\", NULL);\n");
        } else if (mode == 2) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"-P\", \"%d\", NULL);\n", getpid());
        }
        fprintf(file, "        perror(\"Gagal melakukan pkill\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    } else {\n");
        fprintf(file, "        unlink(\"killer\");\n");
        fprintf(file, "        perror(\"Gagal melakukan unlink\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    }\n");
        fprintf(file, "    return 0;\n");
        fprintf(file, "}\n");

        fclose(file);

        pid_t pid = fork();

        if (pid == 0) {
            execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);
            perror("Gagal melakukan kompilasi killer");
            exit(EXIT_FAILURE);
        }
        wait(NULL);

        // Eksekusi killer
        execlp("./killer", "./killer", NULL);
        perror("Gagal mengeksekusi killer");
        exit(EXIT_FAILURE);
    }
}
```

Penjelasan kode:

- `void programKiller(int mode) {`

Fungsi ini didefinisikan dengan tipe pengembalian void dan menerima satu parameter **mode** yang menentukan tipe pembunuhan yang akan dilakukan.

- `FILE *file = fopen("killer.c", "w");`

Fungsi **fopen** digunakan untuk membuka atau membuat file dengan nama "killer.c" dalam mode menulis ("w"). File ini akan digunakan untuk menyimpan kode program "killer".

- `if (file != NULL) {`

Blok ini akan dijalankan jika file berhasil dibuka.

- Penulisan kode C untuk program "killer" menggunakan fungsi **fprintf** untuk menulis ke file "killer.c". Kode ini mencakup pembuatan proses anak, penggunaan **pkill**, dan penghapusan file "killer".

`fprintf(file, "#include <stdio.h>\n");`

`fprintf(file, "#include <unistd.h>\n");`

`fprintf(file, "#include <stdlib.h>\n");`

`fprintf(file, "int main() {\n");`

`fprintf(file, " pid_t child = fork();\n");`

`fprintf(file, " if (child == 0) {\n");`

Blok di atas, kode program killer memulai dengan mengimpor pustaka yang diperlukan dan mendefinisikan fungsi **main**.

- Di dalam **if (child == 0) {**, terdapat kondisi untuk memastikan bahwa kita berada di dalam proses anak:

- `if (mode == 1) {:` Mode 1, membunuh semua proses dengan nama "lukisan".

    - `fprintf(file, " execlp(\"pkill\", \"pkill\", \"lukisan\", NULL);\n");`
    Mengeksekusi perintah pkill lukisan untuk membunuh semua proses dengan nama "lukisan".
- `else if (mode == 2) {`: Mode 2, membunuh proses dengan pid tertentu.

    - `fprintf(file, " execlp(\"pkill\", \"pkill\", \"-P\", \"%d\", NULL);\n", getpid());`
Mengeksekusi perintah **pkill -P <pid>** untuk membunuh proses dengan pid yang diperoleh dari **getpid()**.

- `fprintf(file, " perror(\"Gagal melakukan pkill\");\n");`
Mencetak pesan kesalahan jika perintah **pkill** gagal dieksekusi.

- `fprintf(file, " exit(EXIT_FAILURE);\n");`
Menghentikan proses anak jika terjadi kesalahan.

Blok di dalam **else** adalah bagian yang akan dijalankan oleh proses induk (parent process):

- `fprintf(file, " } else {\n");`
- `fprintf(file, " unlink(\"killer\");\n");`
Menghapus file "killer" dengan menggunakan **unlink**.

- `fprintf(file, " perror(\"Gagal melakukan unlink\");\n");`
Mencetak pesan kesalahan jika penghapusan file gagal.

- `fprintf(file, " exit(EXIT_FAILURE);\n");`
Menghentikan proses anak jika terjadi kesalahan.

- `fprintf(file, " }\n");`
- `fprintf(file, " return 0;\n");`
- `fprintf(file, "}\n");`

- `fclose(file);`
Menutup file "killer.c" setelah selesai menulis.

- `pid_t pid = fork();`

Membuat proses anak baru.

- `if (pid == 0) {`

Blok ini akan dijalankan oleh proses anak.

- `execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);`

Mengeksekusi perintah **gcc -o killer killer.c** untuk mengkompilasi file "killer.c" menjadi sebuah program bernama "killer".

- `perror("Gagal melakukan kompilasi killer");`

Mencetak pesan kesalahan jika kompilasi gagal.

- `exit(EXIT_FAILURE);`

Menghentikan proses anak jika terjadi kesalahan.

- `execlp("./killer", "./killer", NULL);`

Mengeksekusi program "killer" yang telah dikompilasi.

- `perror("Gagal mengeksekusi killer");`

Mencetak pesan kesalahan jika eksekusi program "killer" gagal.

#### 6. Selanjutnya adalah program utama dapat yang dapat dijalankan dalam dua mode: MODE_A dan MODE_B.
Untuk mengaktifkan MODE_A, jalankan program dengan argumen -a.
Untuk MODE_B, jalankan program dengan argumen -b.
Dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Dalam MODE_B, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai.

Kode:
``` c
int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Gunakan: %s -a (untuk MODE_A) atau %s -b (untuk MODE_B)\n", argv[0], argv[0]);
        exit(EXIT_FAILURE);
    }

    int mode;
    if (strcmp(argv[1], "-a") == 0) {
        mode = 1; // MODE_A
    } else if (strcmp(argv[1], "-b") == 0) {
        mode = 2; // MODE_B
    } else {
        printf("Invalid argument. pakai -a untuk MODE_A atau -b untuk MODE_B.\n");
        exit(EXIT_FAILURE);
    }
```

Penjelasan kode:
- `int main(int argc, char *argv[]) {`

**argc** adalah jumlah argumen yang diberikan pada program saat dijalankan, dan **argv** adalah array dari string yang berisi argumen-argumen tersebut.

- `if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {`

Pernyataan **if** ini memeriksa apakah jumlah argumen yang diberikan tidak sama dengan 2 atau apakah argumen kedua (**argv[1]**) bukan sama dengan "-a" dan bukan sama dengan "-b".

- `printf("Gunakan: %s -a (untuk MODE_A) atau %s -b (untuk MODE_B)\n", argv[0], argv[0]);`

Jika kondisi pada poin 2 terpenuhi, maka pesan ini akan dicetak ke layar. Pesan ini memberikan petunjuk kepada pengguna cara menggunakan program dengan argumen yang benar.

- `int mode;`

Deklarasi variabel **mode** yang akan digunakan untuk menyimpan mode yang dipilih.

- `if (strcmp(argv[1], "-a") == 0) {`

Pernyataan **if** ini memeriksa apakah argumen kedua adalah "-a".

- `mode = 1; // MODE_A`

Jika argumen kedua adalah "-a", maka **mode** diatur sebagai 1, yang menunjukkan MODE_A.

- `else if (strcmp(argv[1], "-b") == 0) {`

Pernyataan **else if** ini memeriksa apakah argumen kedua adalah "-b".

- `mode = 2; // MODE_B`

Jika argumen kedua adalah "-b", maka **mode** diatur sebagai 2, yang menunjukkan MODE_B.

- `printf("Invalid argument. pakai -a untuk MODE_A atau -b untuk MODE_B.\n");`

Pesan ini dicetak ke layar untuk memberitahu pengguna bahwa argumen yang diberikan tidak valid.

7. Lalu menambah proses fork dan mengubah menjadi berjalan secara daemon

Kode:
``` c
pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```

#### 8. Selanjutnya adalah bagian program yang melakukan operasi terkait dengan pembuatan folder, pengunduhan gambar, kompresi folder, dan file "killer" untuk menghentikan operasi program.

Kode:
``` c
while (1) {
        char timestamp[20];

        membuatFolderTimestamp(timestamp);

        pid_t childPid = fork();

        if (childPid == 0) {
            int i;
            for (i = 0; i < 15; i++) {
                pid_t downloadPid = fork();
                if (downloadPid == 0) {
                    downloadGambar(timestamp, i);
                    exit(EXIT_SUCCESS);
                } else if (downloadPid < 0) {
                    perror("Fork error");
                    exit(EXIT_FAILURE);
                }
                sleep(5);
            }

            for (i = 0; i < 15; i++) {
                wait(NULL);
            }

            zipFolder(timestamp);
            deleteFolder(timestamp);

            exit(EXIT_SUCCESS);
        } else if (childPid < 0) {
            perror("Fork error");
            exit(EXIT_FAILURE);
        }

        if (mode == 1) {
            // In MODE_A, stop operations when killer program is executed.
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        } else if (mode == 2) {
            // In MODE_B, wait for all child processes to finish before checking for killer program.
            while (wait(NULL) > 0);
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        }
    }
    return 0;
}
```

Penjelasan kode:
- `while (1) {`

Ini adalah loop tak terbatas, yang berarti akan terus berjalan selama kondisi di dalam kurung kurawal {}

- `char timestamp[20];`

Deklarasi array **timestamp** dengan kapasitas untuk menyimpan 20 karakter. Ini akan digunakan untuk menyimpan timestamp.

- `membuatFolderTimestamp(timestamp);`

Memanggil fungsi **membuatFolderTimestamp** untuk membuat folder dengan timestamp yang baru.

- `pid_t childPid = fork();`

Membuat anak proses.

- `if (childPid == 0) {`

Blok ini akan dijalankan oleh proses anak.

- `int i;`

Deklarasi variabel **i** untuk iterasi.

- `for (i = 0; i < 15; i++) {`

Loop untuk melakukan pengunduhan gambar sebanyak 15 kali.

- `pid_t downloadPid = fork();`

Membuat anak proses untuk pengunduhan gambar.

- `if (downloadPid == 0) {`

Blok ini akan dijalankan oleh proses anak yang bertanggung jawab untuk pengunduhan gambar.

- `downloadGambar(timestamp, i);`

Memanggil fungsi **downloadGambar** untuk mengunduh gambar dengan menggunakan timestamp dan indeks i.

- `} else if (downloadPid < 0) {`

Jika pembuatan proses anak gagal.

- `sleep(5);`

Menunggu selama 5 detik sebelum melakukan pengunduhan gambar selanjutnya.

- `for (i = 0; i < 15; i++) {`

Loop untuk menunggu setiap proses anak selesai.

- `wait(NULL);`

Menunggu proses anak selesai.

- `zipFolder(timestamp);`

Memanggil fungsi **zipFolder** untuk mengompresi folder dengan timestamp yang sama.

- `deleteFolder(timestamp);`

Memanggil fungsi **deleteFolder** untuk menghapus folder dengan timestamp yang sama.

- `exit(EXIT_SUCCESS);`

Keluar dari proses anak dengan status keluaran sukses.

- `} else if (childPid < 0) {`

Jika pembuatan proses anak gagal.

- `perror("Fork error");`

Menangani pesan error jika ada kesalahan pada proses pembuatan anak.

- `if (mode == 1) {`

Jika mode adalah MODE_A.

- `if (access("killer", F_OK) != -1) {`

Memeriksa apakah file "killer" ada di direktori. Jika ada, maka program akan berhenti.

- `sleep(30);`

Menunggu selama 30 detik sebelum melakukan iterasi berikutnya dalam loop utama.

- `else if (mode == 2) {`

Jika mode adalah MODE_B.

- `while (wait(NULL) > 0);`

Menunggu hingga semua proses anak selesai sebelum melanjutkan.

- `if (access("killer", F_OK) != -1) {`

Memeriksa apakah file "killer" ada di direktori. Jika ada, maka program akan berhenti.

- `sleep(30);`

Menunggu selama 30 detik sebelum melakukan iterasi berikutnya dalam loop utama.

## Kendala
- Foldernya tidak mau terbuat tiap 30 detik, akan tetapi folder terbuat setelah proses download 15 gambar selesai, lalu akan terbuat folder baru setelah 30 detik.  
- Nama file gambarnya tidak berformat timestamp.
- Program killer MODE_B nya tidak sesuai dengan perintah, prosesnya sama saja dengan MODE_A

## Revisi
Merevisi pada bagin fungsi zipFolder

**Script full:**
``` c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <zlib.h>

void zipFolder(char* timestamp) {
    char command[100];
    sprintf(command, "zip -r %s.zip %s", timestamp, timestamp);
    pid_t pid = fork();

    if (pid == 0) {
        execlp("zip", "zip", "-r", strcat(timestamp, ".zip"), timestamp, NULL);
        perror("Gagal melakukan zip");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}


void deleteFolder(char* timestamp) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-r", timestamp, NULL);
        perror("Gagal menghapus folder");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}

void downloadGambar(char* timestamp, int i) {
    char url[100];
    char namaFile[100];
    time_t waktu = time(NULL);
    int ukuranGambar = (waktu % 1000) + 50;

    sprintf(url, "https://source.unsplash.com/%dx%d", ukuranGambar, ukuranGambar);
    sprintf(namaFile, "%s/image%d.jpg", timestamp, i);

    if (execlp("curl", "curl", "-s", "-o", namaFile, url, (char *)NULL) == -1) {
        perror("execlp");
        exit(EXIT_FAILURE);
    }
}

void membuatFolderTimestamp(char* timestamp) {
    time_t waktu;
    struct tm *info_tm;

    do {
        time(&waktu);
        info_tm = localtime(&waktu);
        strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", info_tm);

        if (mkdir(timestamp, 0777) == -1) {
            if (errno != EEXIST) {
                perror("Gagal membuat folder");
                exit(EXIT_FAILURE);
            }
        } else {
            printf("Folder %s sukses terbuat.\n", timestamp);
            break;
        }

        sleep(30);
    } while (1);
}

void programKiller(int mode) {
    FILE *file = fopen("killer.c", "w");

    if (file != NULL) {
        fprintf(file, "#include <stdio.h>\n");
        fprintf(file, "#include <unistd.h>\n");
        fprintf(file, "#include <stdlib.h>\n");
        fprintf(file, "\n");
        fprintf(file, "int main() {\n");
        fprintf(file, "    pid_t child = fork();\n");
        fprintf(file, "    if (child == 0) {\n");
        if (mode == 1) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"lukisan\", NULL);\n");
        } else if (mode == 2) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"-P\", \"%d\", NULL);\n", getpid());
        }
        fprintf(file, "        perror(\"Gagal melakukan pkill\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    } else {\n");
        fprintf(file, "        unlink(\"killer\");\n");
        fprintf(file, "        perror(\"Gagal melakukan unlink\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    }\n");
        fprintf(file, "    return 0;\n");
        fprintf(file, "}\n");

        fclose(file);

        pid_t pid = fork();

        if (pid == 0) {
            execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);
            perror("Gagal melakukan kompilasi killer");
            exit(EXIT_FAILURE);
        }
        wait(NULL);

        // Eksekusi killer
        execlp("./killer", "./killer", NULL);
        perror("Gagal mengeksekusi killer");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Gunakan: %s -a (untuk MODE_A) atau %s -b (untuk MODE_B)\n", argv[0], argv[0]);
        exit(EXIT_FAILURE);
    }

    int mode;
    if (strcmp(argv[1], "-a") == 0) {
        mode = 1; // MODE_A
    } else if (strcmp(argv[1], "-b") == 0) {
        mode = 2; // MODE_B
    } else {
        printf("Invalid argument. pakai -a untuk MODE_A atau -b untuk MODE_B.\n");
        exit(EXIT_FAILURE);
    }

    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        char timestamp[20];

        membuatFolderTimestamp(timestamp); // Pindahkan ini ke dalam anak proses

        pid_t childPid = fork();

        if (childPid == 0) {
            int i;
            for (i = 0; i < 15; i++) {
                pid_t downloadPid = fork();
                if (downloadPid == 0) {
                    downloadGambar(timestamp, i);
                    exit(EXIT_SUCCESS);
                } else if (downloadPid < 0) {
                    perror("Fork error");
                    exit(EXIT_FAILURE);
                }
                sleep(5);
            }

            for (i = 0; i < 15; i++) {
                wait(NULL);
            }

            zipFolder(timestamp);
            deleteFolder(timestamp);

            exit(EXIT_SUCCESS);
        } else if (childPid < 0) {
            perror("Fork error");
            exit(EXIT_FAILURE);
        }

        if (mode == 1) {
            // In MODE_A, stop operations when killer program is executed.
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        } else if (mode == 2) {
            // In MODE_B, wait for all child processes to finish before checking for killer program.
            while (wait(NULL) > 0);
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        }
    }
    return 0;
}
```

#### Proses menjalankan program

1. Menjalankan program dengan command (**$ ./lukisan -a atau -b**).

![run lukisan](gambar/run-lukisan.png)

2. Cek isi direktori (**$ ls**), akan terdapat folder baru yang terbuat.

![folder terbuat](gambar/folder-terbuat.png)

3. Cek isi folder dengan (**$ cd namafoldertimestamp**) bahwa telah terbuat. Setelah proses download 15 gambar terpenuhi, maka folder tersebut akan di zip terlebih dahulu sebelum dihapus folder timestampnya. Folder baru akan terus terbuat per 30 detik.

![cek folder](gambar/cek-folder.png)

4. Cek bahwa folder zip terbuat ketika mendownload 15 gambarnya selesai (**$ ls**).

![cek zip](gambar/cek-zip.png)

5. Kompile program "**killer**" menggunakan perintah gcc, lalu akan terbuat file eksekusi bernama "**killer**".

![kompile killer](gambar/kompile-killer.png)

6. Jalankan program "**killer**", lalu file eksekusinya akan terhapus sendiri ketika dijalankan programnya dan proses program **lukisan** akan terhenti sesuai dengan mode pilihan yang dipilih.

![run killer](gambar/run-killer.png)

# Soal 4

Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT
dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari
ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk
meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

## Cara Pengerjaan

**A.** Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system().

1. Membuat folder sisop_infected dan quarantine
```bash
mkdir sisop_infected
mkdir quarantine
```

2. Membuat program untuk mendownload file extensions.cavs
```bash
touch extensions.csv
chmod +x extensions.csv
nano extensions.csv
```

3. Tambahkan kode berikut
```bash
extensions="extensions.csv"

wget -O "$extensions" --no-check-certificate "https://drive.google.com/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5"
```

4. Jalankan program tersebut untuk mendownload file
```bash
./extensions.sh
```

5. Membuat file antivirus.c
```bash
touch antivirus.c
```

6. Membuat agar program dapat memindahkan file sesuai dengan ekstensinya
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <signal.h>
#include <time.h>
#include <syslog.h>

void pindahFile(const char* nama_file) {
    char path_asal[256];
    char path_tujuan[256];

    snprintf(path_asal, sizeof(path_asal), "sisop_infected/%s", nama_file);
    snprintf(path_tujuan, sizeof(path_tujuan), "karantina/%s", nama_file);

    if (rename(path_asal, path_tujuan) != 0) {
        perror("Error dalam memindahkan file");
    } else {
        printf("Memindahkan file: %s\n", nama_file);
    }
}

void bacaFileEkstensi() {
    FILE *file_ekstensi = fopen("ekstensi.csv", "r");
    if (file_ekstensi) {
        char line[256];
        while (fgets(line, sizeof(line), file_ekstensi)) {
            char *ekstensi = strtok(line, ",");
            if (ekstensi) {
                num_ekstensi++;
                char **temp = (char **)realloc(ekstensi_diperbolehkan, num_ekstensi * sizeof(char *));
                if (temp) {
                    ekstensi_diperbolehkan = temp;
                    ekstensi_diperbolehkan[num_ekstensi - 1] = strdup(ekstensi);
                } else {
                    perror("Error dalam alokasi ulang memori");
                    exit(1);
                }
            }
        }
        fclose(file_ekstensi);
    } else {
        perror("Error membuka ekstensi.csv");
    }
}

int isEkstensiDiperbolehkan(const char *ekstensi) {
    for (int i = 0; i < num_ekstensi; i++) {
        if (strcmp(ekstensi, ekstensi_diperbolehkan[i]) == 0) {
            return 1;
        }
    }
    return 0;
}
```

**B.** Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log.

1. Tambahkan pada skrip extensions.sh agar dapat melakukan decrypt
```bash
tail -n +9 "$extensions" | tr 'A-Za-z' 'N-ZA-Mn-za-m' > temp.txt
mv temp.txt "$extensions"
```

2. Tambahkan kode pada program agar dapat mencatat log
```c
void tambahLog(const char* nama_file, const char* aksi) {
    time_t waktu_sekarang;
    struct tm *info;
    time(&waktu_sekarang);
    info = localtime(&waktu_sekarang);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", info);

    char tingkat_keamanan_str[20];
    if (tingkat_keamanan == 0) {
        strcpy(tingkat_keamanan_str, "rendah");
    } else if (tingkat_keamanan == 1) {
        strcpy(tingkat_keamanan_str, "menengah");
    } else if (tingkat_keamanan == 2) {
        strcpy(tingkat_keamanan_str, "tinggi");
    }

    FILE *logFile = fopen("virus.log", "a");
    if (logFile) {
        fprintf(logFile, "[%d][%s] - %s - %s. (untuk tingkat %s)\n", getpid(), timestamp, nama_file, aksi, tingkat_keamanan_str);
        fclose(logFile);
    } else {
        perror("Error membuka virus.log");
    }
}
```

**C.** Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.

1. Tambahkan pada program agar dapat memeriksa setiap detik
```c
while (1) {
    struct dirent *entry;
    DIR *dir = opendir("sisop_infected");

    if (dir == NULL) {
        perror("Error dalam membuka direktori");
        return 1;
    }

    while ((entry = readdir(dir))) {
        if (entry->d_type == DT_REG) {
            char *nama_file = entry->d_name;
            char *ekstensi = strrchr(nama_file, '.');

            if (ekstensi != NULL) {
                ekstensi++;
            }

            if (tingkat_keamanan == 0) {
                tambahLog(nama_file, "Me-log file.");
            } else if (tingkat_keamanan == 1) {
                if (isEkstensiDiperbolehkan(ekstensi)) {
                    pindahFile(nama_file);
                    tambahLog(nama_file, "Dipindahkan ke karantina.");
                } else {
                    tambahLog(nama_file, "Me-log file.");
                }
            } else if (tingkat_keamanan == 2) {
                if (isEkstensiDiperbolehkan(ekstensi)) {
                    hapusFile(nama_file);
                    tambahLog(nama_file, "Menghapus file.");
                } else {
                   tambahLog(nama_file, "Me-log file.");
                }
             }
        }
    }

    closedir(dir);

    sleep(1);
}
```

2. Tambahkan pada program agar berjalan secara daemon
```c
pid_t pid, sid;

pid = fork();

if (pid < 0) {
    exit(EXIT_FAILURE);
}

if (pid > 0) {
    exit(EXIT_SUCCESS);
}

umask(0);

sid = setsid();

if (sid < 0) {
    exit(EXIT_FAILURE);
}

close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);

openlog("antivirus", LOG_PID, LOG_DAEMON);
```

## Kendala
Kendala dalam membuat program menjadi daemon
Kendala dalam mengatur sinyal program
Kendala dalam pencatatan log
Kendala dalam mengecek ekstensi file

## Revisi
**D.** Choco juga membutuhkan level-level keamanan antivirus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari
antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk
mengganti level keamanan antivirus dengan mengirim sinyal ke daemon.
Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk
"medium", dan SIGRTMIN untuk mode "hard".

1. Tambahkan pada program agar dapat melakukan sesuai dengan tingkat keamanannya
```c
void hapusFile(const char* nama_file) {
    char path_file[256];
    snprintf(path_file, sizeof(path_file), "sisop_infected/%s", nama_file);

    if (remove(path_file) != 0) {
        perror("Error dalam menghapus file");
    } else {
        printf("Menghapus file: %s\n", nama_file);
    }
}

void handleSignal(int signum) {
    char tingkat_keamanan_baru[20];
    if (signum == SIGUSR1) {
        tingkat_keamanan = 0;
        strcpy(tingkat_keamanan_baru, "rendah");
    } else if (signum == SIGUSR2) {
        tingkat_keamanan = 1;
        strcpy(tingkat_keamanan_baru, "menengah");
    } else if (signum == SIGRTMIN) {
        tingkat_keamanan = 2;
        strcpy(tingkat_keamanan_baru, "tinggi");
    } 

    char pesan_log[100];
    snprintf(pesan_log, sizeof(pesan_log), "Tingkat keamanan diganti menjadi %s", tingkat_keamanan_baru);

    tambahLog("sistem", pesan_log);
}

int main(int argc, char *argv[]) {
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    openlog("antivirus", LOG_PID, LOG_DAEMON);

    
    if (argc != 3) {
        printf("Penggunaan: ./antivirus -p (tingkat_keamanan)\n");
        printf("Tingkat keamanan: 0 (rendah), 1 (menengah), 2 (tinggi)\n");
        return 1;
    }

    tingkat_keamanan = atoi(argv[2]);

    signal(SIGUSR1, handleSignal);
    signal(SIGUSR2, handleSignal);
    signal(SIGRTMIN, handleSignal);

    bacaFileEkstensi();

    while (1) {
        struct dirent *entry;
        DIR *dir = opendir("sisop_infected");

        if (dir == NULL) {
            perror("Error dalam membuka direktori");
            return 1;
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) {
                char *nama_file = entry->d_name;
                char *ekstensi = strrchr(nama_file, '.');

                if (ekstensi != NULL) {
                    ekstensi++;
                }

                if (tingkat_keamanan == 0) {
                    tambahLog(nama_file, "Me-log file.");
                } else if (tingkat_keamanan == 1) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        pindahFile(nama_file);
                        tambahLog(nama_file, "Dipindahkan ke karantina.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                } else if (tingkat_keamanan == 2) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        hapusFile(nama_file);
                        tambahLog(nama_file, "Menghapus file.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                }
            }
        }

        closedir(dir);

        sleep(1);
    }

    for (int i = 0; i < num_ekstensi; i++) {
        free(ekstensi_diperbolehkan[i]);
    }
    free(ekstensi_diperbolehkan);

    closelog();

    return 0;
}
```

**E.**  Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

1. Tambahkan sinyal untuk menghentikan program
```c
else if (signum == SIGTERM) {
        exit(0);
    }
signal(SIGTERM, handleSignal);
```

- **Hasil output program**<br>
![gambar output](gambar/output_soal4.png)
<br>

- **Full kode program<br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <signal.h>
#include <time.h>
#include <syslog.h>

int tingkat_keamanan = 0;
char **ekstensi_diperbolehkan = NULL;
int num_ekstensi = 0;

void pindahFile(const char* nama_file) {
    char path_asal[256];
    char path_tujuan[256];

    snprintf(path_asal, sizeof(path_asal), "sisop_infected/%s", nama_file);
    snprintf(path_tujuan, sizeof(path_tujuan), "karantina/%s", nama_file);

    if (rename(path_asal, path_tujuan) != 0) {
        perror("Error dalam memindahkan file");
    } else {
        printf("Memindahkan file: %s\n", nama_file);
    }
}

void hapusFile(const char* nama_file) {
    char path_file[256];
    snprintf(path_file, sizeof(path_file), "sisop_infected/%s", nama_file);

    if (remove(path_file) != 0) {
        perror("Error dalam menghapus file");
    } else {
        printf("Menghapus file: %s\n", nama_file);
    }
}

void tambahLog(const char* nama_file, const char* aksi) {
    time_t waktu_sekarang;
    struct tm *info;
    time(&waktu_sekarang);
    info = localtime(&waktu_sekarang);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", info);

    char tingkat_keamanan_str[20];
    if (tingkat_keamanan == 0) {
        strcpy(tingkat_keamanan_str, "rendah");
    } else if (tingkat_keamanan == 1) {
        strcpy(tingkat_keamanan_str, "menengah");
    } else if (tingkat_keamanan == 2) {
        strcpy(tingkat_keamanan_str, "tinggi");
    }

    FILE *logFile = fopen("virus.log", "a");
    if (logFile) {
        fprintf(logFile, "[%d][%s] - %s - %s. (untuk tingkat %s)\n", getpid(), timestamp, nama_file, aksi, tingkat_keamanan_str);
        fclose(logFile);
    } else {
        perror("Error membuka virus.log");
    }
}

void handleSignal(int signum) {
    char tingkat_keamanan_baru[20];
    if (signum == SIGUSR1) {
        tingkat_keamanan = 0;
        strcpy(tingkat_keamanan_baru, "rendah");
    } else if (signum == SIGUSR2) {
        tingkat_keamanan = 1;
        strcpy(tingkat_keamanan_baru, "menengah");
    } else if (signum == SIGRTMIN) {
        tingkat_keamanan = 2;
        strcpy(tingkat_keamanan_baru, "tinggi");
    } else if (signum == SIGTERM) {
        exit(0);
    }

    char pesan_log[100];
    snprintf(pesan_log, sizeof(pesan_log), "Tingkat keamanan diganti menjadi %s", tingkat_keamanan_baru);

    tambahLog("sistem", pesan_log);
}

void bacaFileEkstensi() {
    FILE *file_ekstensi = fopen("ekstensi.csv", "r");
    if (file_ekstensi) {
        char line[256];
        while (fgets(line, sizeof(line), file_ekstensi)) {
            char *ekstensi = strtok(line, ",");
            if (ekstensi) {
                num_ekstensi++;
                char **temp = (char **)realloc(ekstensi_diperbolehkan, num_ekstensi * sizeof(char *));
                if (temp) {
                    ekstensi_diperbolehkan = temp;
                    ekstensi_diperbolehkan[num_ekstensi - 1] = strdup(ekstensi);
                } else {
                    perror("Error dalam alokasi ulang memori");
                    exit(1);
                }
            }
        }
        fclose(file_ekstensi);
    } else {
        perror("Error membuka ekstensi.csv");
    }
}

int isEkstensiDiperbolehkan(const char *ekstensi) {
    for (int i = 0; i < num_ekstensi; i++) {
        if (strcmp(ekstensi, ekstensi_diperbolehkan[i]) == 0) {
            return 1;
        }
    }
    return 0;
}

int main(int argc, char *argv[]) {
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    openlog("antivirus", LOG_PID, LOG_DAEMON);

    
    if (argc != 3) {
        printf("Penggunaan: ./antivirus -p (tingkat_keamanan)\n");
        printf("Tingkat keamanan: 0 (rendah), 1 (menengah), 2 (tinggi)\n");
        return 1;
    }

    tingkat_keamanan = atoi(argv[2]);

    signal(SIGUSR1, handleSignal);
    signal(SIGUSR2, handleSignal);
    signal(SIGRTMIN, handleSignal);
    signal(SIGTERM, handleSignal);

    bacaFileEkstensi();

    while (1) {
        struct dirent *entry;
        DIR *dir = opendir("sisop_infected");

        if (dir == NULL) {
            perror("Error dalam membuka direktori");
            return 1;
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) {
                char *nama_file = entry->d_name;
                char *ekstensi = strrchr(nama_file, '.');

                if (ekstensi != NULL) {
                    ekstensi++;
                }

                if (tingkat_keamanan == 0) {
                    tambahLog(nama_file, "Me-log file.");
                } else if (tingkat_keamanan == 1) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        pindahFile(nama_file);
                        tambahLog(nama_file, "Dipindahkan ke karantina.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                } else if (tingkat_keamanan == 2) {
                    if (isEkstensiDiperbolehkan(ekstensi)) {
                        hapusFile(nama_file);
                        tambahLog(nama_file, "Menghapus file.");
                    } else {
                        tambahLog(nama_file, "Me-log file.");
                    }
                }
            }
        }

        closedir(dir);

        sleep(1);
    }

    for (int i = 0; i < num_ekstensi; i++) {
        free(ekstensi_diperbolehkan[i]);
    }
    free(ekstensi_diperbolehkan);

    closelog();

    return 0;
}
```
