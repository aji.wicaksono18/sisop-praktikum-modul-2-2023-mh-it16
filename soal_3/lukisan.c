#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <zlib.h>

void zipFolder(char* timestamp) {
    char command[100];
    sprintf(command, "zip -r %s.zip %s", timestamp, timestamp);
    pid_t pid = fork();

    if (pid == 0) {
        execlp("zip", "zip", "-r", strcat(timestamp, ".zip"), timestamp, NULL);
        perror("Gagal melakukan zip");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}


void deleteFolder(char* timestamp) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-r", timestamp, NULL);
        perror("Gagal menghapus folder");
        exit(EXIT_FAILURE);
    }
    wait(NULL);
}

void downloadGambar(char* timestamp, int i) {
    char url[100];
    char namaFile[100];
    time_t waktu = time(NULL);
    int ukuranGambar = (waktu % 1000) + 50;

    sprintf(url, "https://source.unsplash.com/%dx%d", ukuranGambar, ukuranGambar);
    sprintf(namaFile, "%s/image%d.jpg", timestamp, i);

    if (execlp("curl", "curl", "-s", "-o", namaFile, url, (char *)NULL) == -1) {
        perror("execlp");
        exit(EXIT_FAILURE);
    }
}

void membuatFolderTimestamp(char* timestamp) {
    time_t waktu;
    struct tm *info_tm;

    do {
        time(&waktu);
        info_tm = localtime(&waktu);
        strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", info_tm);

        if (mkdir(timestamp, 0777) == -1) {
            if (errno != EEXIST) {
                perror("Gagal membuat folder");
                exit(EXIT_FAILURE);
            }
        } else {
            printf("Folder %s sukses terbuat.\n", timestamp);
            break;
        }

        sleep(30);
    } while (1);
}

void programKiller(int mode) {
    FILE *file = fopen("killer.c", "w");

    if (file != NULL) {
        fprintf(file, "#include <stdio.h>\n");
        fprintf(file, "#include <unistd.h>\n");
        fprintf(file, "#include <stdlib.h>\n");
        fprintf(file, "\n");
        fprintf(file, "int main() {\n");
        fprintf(file, "    pid_t child = fork();\n");
        fprintf(file, "    if (child == 0) {\n");
        if (mode == 1) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"lukisan\", NULL);\n");
        } else if (mode == 2) {
            fprintf(file, "        execlp(\"pkill\", \"pkill\", \"-P\", \"%d\", NULL);\n", getpid());
        }
        fprintf(file, "        perror(\"Gagal melakukan pkill\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    } else {\n");
        fprintf(file, "        unlink(\"killer\");\n");
        fprintf(file, "        perror(\"Gagal melakukan unlink\");\n");
        fprintf(file, "        exit(EXIT_FAILURE);\n");
        fprintf(file, "    }\n");
        fprintf(file, "    return 0;\n");
        fprintf(file, "}\n");

        fclose(file);

        pid_t pid = fork();

        if (pid == 0) {
            execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);
            perror("Gagal melakukan kompilasi killer");
            exit(EXIT_FAILURE);
        }
        wait(NULL);

        // Eksekusi killer
        execlp("./killer", "./killer", NULL);
        perror("Gagal mengeksekusi killer");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Gunakan: %s -a (untuk MODE_A) atau %s -b (untuk MODE_B)\n", argv[0], argv[0]);
        exit(EXIT_FAILURE);
    }

    int mode;
    if (strcmp(argv[1], "-a") == 0) {
        mode = 1; // MODE_A
    } else if (strcmp(argv[1], "-b") == 0) {
        mode = 2; // MODE_B
    } else {
        printf("Invalid argument. pakai -a untuk MODE_A atau -b untuk MODE_B.\n");
        exit(EXIT_FAILURE);
    }

    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        char timestamp[20];

        membuatFolderTimestamp(timestamp); 

        pid_t childPid = fork();

        if (childPid == 0) {
            int i;
            for (i = 0; i < 15; i++) {
                pid_t downloadPid = fork();
                if (downloadPid == 0) {
                    downloadGambar(timestamp, i);
                    exit(EXIT_SUCCESS);
                } else if (downloadPid < 0) {
                    perror("Fork error");
                    exit(EXIT_FAILURE);
                }
                sleep(5);
            }

            for (i = 0; i < 15; i++) {
                wait(NULL);
            }

            zipFolder(timestamp);
            deleteFolder(timestamp);

            exit(EXIT_SUCCESS);
        } else if (childPid < 0) {
            perror("Fork error");
            exit(EXIT_FAILURE);
        }

        if (mode == 1) {
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        } else if (mode == 2) {
            while (wait(NULL) > 0);
            if (access("killer", F_OK) != -1) {
                break;
            }
            sleep(30);
        }
    }
    return 0;
}
